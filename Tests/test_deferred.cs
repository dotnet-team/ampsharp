﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
namespace AMP.Tests
{
    using System;
    using NUnit.Framework;
    using NUnit.Framework.Constraints;
    using System.Text;

    [TestFixture]
    public class DeferredResponseTests
    {
        AMP.DeferredResponse deferred;
        [SetUp]
        public void Init()
        {
            deferred = new AMP.DeferredResponse();
        }

        [Test]
        public void set_then_callback()
        {
            Object callbackArg = null;
            deferred.setCallback(obj => { callbackArg = obj; });

            Assert.That(callbackArg, Is.Null);

            deferred.respond((Object)"hello");

            Assert.That("hello", Is.EqualTo((string)callbackArg));
            
        }

        [Test]
        public void callback_then_set()
        {
            Object callbackArg = null;
            deferred.respond((Object)"hello");

            Assert.That(callbackArg, Is.Null);

            deferred.setCallback(obj => { callbackArg = obj; });

            Assert.That("hello", Is.EqualTo((string)callbackArg));
        }

    }
}
