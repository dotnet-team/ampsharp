AMP# - The client/server AMP library for all .NET runtimes.

AMP Website: http://amp-protocol.net

AMP# Website: http://amp-protocol.net/Ampsharp

Code is hosted on Launchpad: https://launchpad.net/ampsharp

The solution file should open in Visual Studio or Mono Develop.

Author: Eric P. Mangold - teratorn@gmail.com - teratorn on Freenode IRC

=== LOGGING ===

A file 'AMPSharp.dll.log4net' should be placed in the build directory
along side AMPSharp.dll. Edit this file to configure logging options.

By default it has the logging level set to DEBUG which will produce
*a lot* of log output... probably a lot more than you want for production.

Set the log level to something sensible like ERROR for production.
(Currently, the code only uses two levels, DEBUG and ERROR)


=== DOCS ===

Check out the example programs in doc/examples

See the AMP docs on amp-protocol.net

Read the source code :)


=== BUILDING ===

You will need log4net installed to build the AMPSharp project.

MonoDevelop or Visual Studio should build the whole Solution or individual
Projects just fine.

Alternatively use Mono's `xbuild` utility to do it on the command-line:

xbuild AMPSharp.csproj

Or build the whole solution:

xbuild AMPSharp.sln


=== BUILDING WITH MONO ON THE COMMAND-LINE ===

To build the assembly using Mono, just type `make'.


=== BUILDING .DEB PACKAGES ===

On Debian, Ubuntu or other derivatives, type a command like this:

fakeroot debian/rules binary

You may be missing some build dependencies - to be sure you have them all
installed type `sudo mk-build-deps -i'


=== UNIT TESTS ===

To build the Tests project you will need NUnit installed.

NUnit 2.5 or higher is required. 

NOTE: Tests/Tests.csproj references nunit.framework 2.5.10.11092 *specifically*.
Change this to match your installed version of nunit 2.5. Find it with:

    gacutil -l nunit.framework

After compiling the Tests.dll assembly it may be "run" with any of the NUnit
test runners as usual.

===============

Have fun!
