﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
using System;
using System.Collections;
using System.Collections.Generic;

namespace AMP
{
    internal class Parameter
    {
        internal string name;
        internal IAmpType type;

        public Parameter(string name, IAmpType ampType)
        {
            this.name = name;
            type = ampType;
        }
    }

    public class Command
    {
        // The return value of Responder must be in the form of Object since we allow you
        // to return a Msg (if responding synchronously) or a DeferredResponse (if you need to
        // respond asyncronously).
        public delegate Object Responder(Msg msg, Object state);

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("AMP");

        public bool RequiresAnswer { get; set; }

        private string name;
        internal List<Parameter> arguments;
        internal List<Parameter> response;
        internal Dictionary<System.Type, byte[]> errors = new Dictionary<System.Type, byte[]>();

        public string Name
        {
            get { return name; }
        }

        public Command()
        {
            arguments = new List<Parameter>();
            response = new List<Parameter>();

            // These are the errors that every Command will know about, since they
            // are part of the AMP protocol.
            errors.Add(typeof(Error.UnknownRemoteError), Error.Codes.UNKNOWN_ERROR_CODE);
            errors.Add(typeof(Error.UnhandledCommand), Error.Codes.UNHANDLED_ERROR_CODE);

            RequiresAnswer = true; // default. changeable.
        }

        internal Command(List<Parameter> args)
            : this()
        {
            arguments = args;
        }

        public Command(string name)
            : this()
        {
            this.name = name;
        }

        public void AddArgument(string keyName, IAmpType type)
        {
            arguments.Add(new Parameter(keyName, type));
        }

        public void AddResponse(string keyName, IAmpType type)
        {
            response.Add(new Parameter(keyName, type));
        }

        public void AddError(System.Type errorClass, byte[] errorCode)
        {
            errors.Add(errorClass, errorCode);
        }

        internal Msg_Raw ToRawMessage(Msg typed_msg, MsgType msgType)
        {
            List<Parameter> paramList = (msgType == MsgType.REQUEST ? arguments : response);
            var result = new Msg_Raw();
            foreach (Parameter param in paramList)
            {
                if (!typed_msg.ContainsKey(param.name))
                {
                    log.ErrorFormat("Command {0}: paramter \"{1}\" not found in msg!", name, param.name);
                    throw new Error.ParameterNotFound();
                }
                result.Add(param.name, param.type.ToAmpBytes(typed_msg[param.name]));
            }
            return result;
        }

        internal Msg ToTypedMessage(Msg_Raw raw_msg, MsgType msgType)
        {
            List<Parameter> paramList = (msgType == MsgType.REQUEST ? arguments : response);
            var result = new Msg();
            foreach (Parameter param in paramList)
            {
                if (!raw_msg.ContainsKey(param.name))
                {
                    log.ErrorFormat("Command {0}: paramter \"{1}\" not found in msg!", name, param.name);
                    throw new Error.ParameterNotFound();
                }
                result.Add(param.name, param.type.FromAmpBytes(raw_msg[param.name]));
            }
            return result;
        }
    }

    internal enum MsgType
    {
        REQUEST,
        RESPONSE
    }
}
