﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
using System;
using System.Collections.Generic;

namespace AMP
{
    // Container class for a TYPED AMP message. The actual values will be instances of the specific
    // types that have been defined for the fields. This class is what client code will use to handle
    // AMP requests and responses.
    public class Msg : Dictionary<string, Object>
    {
    }

    // Container class for a raw AMP "box"... this always maps field names to byte arrays.
    // We convert back and forth between this class and the TYPED version of an AMP box, Msg,
    // using the Command class which knows how to map fields to types.
    internal class Msg_Raw : Dictionary<string, byte[]>
    {
        internal static void printBytes(byte[] bytes)
        {
            foreach (byte b in bytes)
            {
                Console.Write(System.String.Format("\\x{0:x2}", b));
            }
            Console.WriteLine();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Msg_Raw))
            {
                return false;
            }
            return this == (Msg_Raw)obj;

        }

        public static bool operator ==(Msg_Raw a, Msg_Raw b)
        {
            if (a.Count != b.Count)
            {
                return false;
            }
            foreach (string key in a.Keys)
            {
                if (!b.ContainsKey(key))
                {
                    return false;
                }
                if (a[key].Length != b[key].Length) return false;
                for (int i = 0; i < a[key].Length; i++)
                {
                    if (a[key][i] != b[key][i]) return false;
                }
            }
            return true;
        }


        public static bool operator !=(Msg_Raw a, Msg_Raw b)
        {
            return !(a == b);
        }
    }

    internal struct Ask_Info
    {
        public int askKey;
        public Command cmd;
        public SimpleAsyncResult ar;

        public Ask_Info(int askKey, Command cmd, SimpleAsyncResult ar)
        {
            this.askKey = askKey;
            this.cmd = cmd;
            this.ar = ar;
        }
    }

}
