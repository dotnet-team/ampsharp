﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
using System;
using System.Text;

namespace AMP
{
    internal class BytesUtil
    {
        public static bool AreEqual(byte[] x, byte[] y)
        {
            if (x.Length != y.Length) return false;
            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] != y[i]) return false;
            }
            return true;
        }
        public static string ToHexStr(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                if (b < 32 || b > 126)
                {
                    sb.AppendFormat("\\x{0:x2}", b);
                }
                else
                {
                    if (b == '\\')
                    {
                        sb.Append("\\\\");
                    }
                    else
                    {
                        sb.Append((char)b);
                    }
                }
            }
            return sb.ToString();
        }
    }
}
