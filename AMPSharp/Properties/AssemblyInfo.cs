﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
using System.Reflection;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AMPSharp")]
[assembly: AssemblyDescription("A client/server implementation of AMP, the Asynchronous Messaging Protocol, for .NET. ")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("AMPSharp")]
[assembly: AssemblyCopyright("Copyright © 2008-2011, Eric P. Mangold <eric@teratorn.org>")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ce8a23dd-62f6-4234-9028-262cbb0729c3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.4")]
[assembly: AssemblyFileVersion("2.0.4")]
[assembly: AssemblyKeyFileAttribute("ampsharp.snk")]

// Make `internal' types visibile to the Tests assembly
[assembly: InternalsVisibleTo("Tests, PublicKey=002400000480000094000000060200000024000" +
                                               "05253413100040000110000002b7425c119b7fc" +
                                               "48b02d2339b40e844ac04275f2c64d70600a218" +
                                               "04671ec589cb58033d67632f96a66b2b4e2d2b4" +
                                               "ee32d9ef443fcef3770db0da85836746b2f1f0f" +
                                               "fff5438846af2ecd7d69bbabb247731083c160d" +
                                               "7ea9344562764d96ee1d759ba560571e6d028c1" +
                                               "aef494d3c0e4e7c3dc2789a8393f5824d0fa4ec" +
                                               "82ebcfca")]

