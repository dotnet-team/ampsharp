﻿/* Copyright (c) 2008-2011 - Eric P. Mangold
 * Released under the terms of the MIT/X11 license - see LICENSE.txt */
using System;
using System.Net.Sockets;
using System.Text;

namespace AMP.Error
{
    public class Codes
    {
        public static byte[] UNKNOWN_ERROR_CODE = Encoding.UTF8.GetBytes("UNKNOWN");
        public static byte[] UNHANDLED_ERROR_CODE = Encoding.UTF8.GetBytes("UNHANDLED");
    }

    public class StreamClosedException : SocketException
    {
    }

    public class AmpError : Exception
    {
        public AmpError(string description)
            : base(description)
        {
        }

        public AmpError()
        {
        }
    }

    public class ProtocolError : AmpError
    {
    }
    public class MessageEmptyError : AmpError
    {
    }
    public class InvalidKeySizeError : AmpError
    {
    }
    public class InvalidValueSizeError : AmpError
    {
    }
    public class ParameterNotFound : AmpError
    {
    }
    public class TypeDecodeError : AmpError
    {
    }

    public class RemoteAmpError : AmpError
    {
        public byte[] errorCode;
        public RemoteAmpError() { }
        public RemoteAmpError(byte[] errorCode, string description)
            : base(description)
        {
            this.errorCode = errorCode;
        }
    }
    public class UnknownRemoteError : RemoteAmpError
    {
        public UnknownRemoteError(byte[] errorCode, string description)
            : base(errorCode, description)
        {
        }
    }

    public class UnhandledCommand : RemoteAmpError
    {
        public UnhandledCommand(byte[] errorCode, string description)
            : base(errorCode, description)
        {
        }
    }

    public class ServerTimeout : AmpError
    {
    }

    public class BadResponderReturnValue : AmpError
    {
        public override string ToString()
        {
            return "BadResponderReturnValue: Your responder method must return an AMP.Msg or an AMP.DeferredResponse.";
        }
    }
}
